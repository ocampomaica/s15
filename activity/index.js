console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

/*	let user = {
		firstName: "Maica",
		lastName: "Ocampo",
		userAge: 24,
		hobbies: ['gym', 'walking dogs', 'playing games'],
		workAddress: {
			houseNumber: 32,
			street: "Oakwood St.",
			city: "Dasmarinas",
			state: "Cavite"		
		}
	}

	console.log(user)
*/
	console.log("");

	let firstName = "Maica";
	let lastName ="Ocampo";
	let myAge = 24;
	let hobbies = ["Gym","Playing withdogs","Reading"];


	let workAddress = {
		houseNumber: 123,
		street: "Oakwood Street",
		city: "Dasmarinas",
		state: "Cavite"
	};

	console.log("First Name: "+ firstName);
	console.log("Last Name: "+ lastName);
	console.log("Age: "+ myAge);
	console.log("Hobbies " + hobbies);
	console.log("Address: ");
	console.log(workAddress);


/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	console.log("");
	let name = "Steve Rogers";
	console.log("My full name is " + name);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	//lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);