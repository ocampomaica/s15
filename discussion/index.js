//This is a single line comment

		/*
		This is a
		multiline
		comment
		Shortcut key: ctrl +shift + /
		*/

		//alert("Hello World");
		//console.log("Hello world");
		//console.log(3+2);
		/*let name = 'Zuitt';
		console.log(name);*/

		let name;
		name = "Zuitt";
		name = "Emmanuel";
		console.log(name);

		let age = 22;

		const boilingPoint = 100;
		//boilingPoint = 200;

		console.log(boilingPoint);
		console.log(age+3);
		console.log(name+ 'hello');

		//Boolean
		let isAlive = true;
		console.log(isAlive);

		//Arrays
		let grades = [98,96, 95, 90];

		let batch197 = ["marnel", "jesus"]
		let grade1 = 98;
		let grade2 = 96;
		let grade3 = 95;
		let grade4 = 90;

		console.log(grades[4]);

		let movie = "jaws"
		//console.log(song);
		let isp;
		console.log(isp)

		//null vs undefined
		// Undefined - represents the state of a variable that has been declared but without an assigned value
		// null - used intentionally to express the absence of a value in a declared/initialized variable

		let spouse ="null";
		console.log(spouse);

		//Objects - special kind of data that is used to mimic real world objects/items
		/*
		Syntax:
		let objectName = {
			property1: keyValue1,
			property2: keyValue2,
			property3: keyValue3
		}
		*/

		let myGrades = {
			firstGrading: 98,
			secondGrading: 92,
			thirdGrading: 90,
			fourthGrading: 94.6
		}

		console.log(myGrades);

		let person = {
			fullName: 'Alonzo Cruz',
			age: 25,
			isMarried: false,
			contact: ['0986993096','091534567'],
			address: {
				houseNumber: 345,
				city: 'Manila',				
			}
		}
		
		console.log(person)